package com.glearning.student.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.glearning.student.model.Student;


@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
	
	public List<Student> findByAgeBetween(int min, int max);
	public List<Student> findByNameStartingWith(String prefix);
	
	@Query("select s from Student s where s.email= ?1")
	public Student findStudentByEmailAddress(String emailAddress);

}
