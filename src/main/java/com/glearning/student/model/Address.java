package com.glearning.student.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "address")

@NoArgsConstructor
@Setter
@Getter
@ToString(exclude = "student")
@EqualsAndHashCode(exclude = "student")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String city;
	private String state;

	@ManyToOne
	@JoinColumn(name="student_id_fk", nullable = false)
	@JsonBackReference
	private Student student;

}
